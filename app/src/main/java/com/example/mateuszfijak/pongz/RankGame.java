package com.example.mateuszfijak.pongz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;


public class RankGame extends Activity
{
    private RankEngine rankEngine;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //set to full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //getting screen resolution
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        //starting the game
        rankEngine = new RankEngine(this, size.x, size.y);
        setContentView(rankEngine);
    }


    @Override
    protected void onPause()
    {
        super.onPause();

        if(!rankEngine.isGameEnded())
        {
            //going back to menu
            Intent intent = new Intent(getApplicationContext(), MainMenu.class);
            startActivity(intent);
            //resuming menu music
            if(!DataStore.isMuted())
            {
                MainMenu.getMediaPlayer().pause();
                MainMenu.getMediaPlayer().release();
                MainMenu.setMediaPlayer(MediaPlayer.create(getApplicationContext(), R.raw.menutheme));
                MainMenu.getMediaPlayer().start();
                MainMenu.setIsPlaying(true);
            }
            this.finish();
        }
    }


    @Override
    protected void onResume()
    {
        //resuming game
        rankEngine.resume();
        super.onResume();
    }


    @Override
    public void onBackPressed()
    {
        if(rankEngine.isPaused())
        {
            //going back to menu
            Intent intent = new Intent(getApplicationContext(), MainMenu.class);
            startActivity(intent);
            //resuming menu music
            if(!DataStore.isMuted())
            {
                MainMenu.getMediaPlayer().pause();
                MainMenu.getMediaPlayer().release();
                MainMenu.setMediaPlayer(MediaPlayer.create(getApplicationContext(), R.raw.menutheme));
                MainMenu.getMediaPlayer().start();
                MainMenu.setIsPlaying(true);
            }
        }
        rankEngine.pause();
    }
}

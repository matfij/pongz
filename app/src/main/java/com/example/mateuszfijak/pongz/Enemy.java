package com.example.mateuszfijak.pongz;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;


public class Enemy
{
    //enemy apperance
    private RectF shape;
    private float sizeX;
    private float sizeY;
    private int color;
    private int borderColor;

    private int points;

    //enemy movement
    private double speed;

    //ball to analize
    private Ball ball;

    //screen size for proper scalling
    private int screenX;
    private int screenY;

    //difficulty
    private int difficulty;
    private double vision;


    public Enemy(int screenX, int screenY, int difficulty)
    {
        //initializing screen size
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing enemy body
        shape = new RectF();
        this.sizeX = screenX/5;
        this.sizeY = screenY/40;
        this.color = color;

        //setting statistics
        points = 0;

        //initializing difficulty
        this.difficulty = difficulty;
        if(difficulty == 1)
        {
            speed = 0.8*screenX;
            this.vision = 0.5*screenY;
            this.borderColor = Color.rgb(135,206,235);
            this.color = Color.rgb(30,144,255);
        }

        if(difficulty == 2)
        {
            speed = 1.35*screenX;
            this.vision = 0.6*screenY;
            this.borderColor = Color.rgb(255,255,200);
            this.color = Color.rgb(255,255,0);
        }

        if(difficulty == 3)
        {
            speed = 2*screenX;
            this.vision = screenY;
            this.vision = 0.7*screenY;
            this.borderColor = Color.rgb(255,20,0);
            this.color = Color.rgb(211,11,11);
        }
    }


    public void reset()
    {
        //moving enemy to the middle of the field
        shape.left = (int) (screenX/2 - sizeX/2);
        shape.top = (int) (0.012*screenY);
        shape.right = (int) (screenX/2 + sizeX/2);
        shape.bottom = (int)(shape.top + sizeY);
    }


    //updating enemy position
    public void update(double fps, Ball ball)
    {
        if (ball.getShape().centerX() > this.shape.centerX() && Math.abs(ball.getAng()) > 100*Math.PI/180 && ball.getShape().centerY() < vision)
        {
            shape.left = (int) (shape.left + speed / fps);
            shape.top = (int) (0.012*screenY);
            shape.bottom = (int)(shape.top + sizeY);
            shape.right = (int) (shape.left + sizeX);
        }

        if (ball.getShape().centerX() < this.shape.centerX() && Math.abs(ball.getAng()) > 100*Math.PI/180 && ball.getShape().centerY() < vision)
        {
            shape.left = (int) (shape.left - speed / fps);
            shape.top = (int) (0.012*screenY);
            shape.bottom = (int)(shape.top + sizeY);
            shape.right = (int) (shape.left + sizeX);
        }
    }

    //drawing enemy
    public void draw(Canvas canvas)
    {
        //initializing ball colorss+
        Paint paint1 = new Paint();
        paint1.setColor(borderColor);
        Paint paint2 = new Paint();
        paint2.setColor(color);

        //drawing edges
        RectF tempRect = new RectF(shape.left + sizeX/10, shape.top + sizeY/10, shape.right - sizeX/10, shape.bottom - sizeY/10);

        //drawing rects as ovals
        canvas.drawOval(shape, paint1);
        canvas.drawOval(tempRect, paint2);
    }


    public RectF getShape() {
        return shape;
    }

    public int getPoints() { return points; }

    public void setPoints(int points) { this.points = points; }
}

package com.example.mateuszfijak.pongz;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class SoloEngine extends SurfaceView implements SurfaceHolder.Callback
{
    DrawThread drawThread;
    UpdateThread updateThread;
    private Background background;

    private int fps;

    private final int gameType = 1;

    private Rocket rocket;
    private Point point;

    private Ball ball;

    private Enemy enemy;

    private int screenX;
    private int screenY;

    private int difficulty;

    private boolean isPaused;


    public SoloEngine(Context context, int screenX, int screenY)
    {
        super(context);

        //adding the callback to the surfaceholder to intercept events
        getHolder().addCallback(this);

        //makeing gamePanel focusable so it can handle events
        setFocusable(true);

        //initializing screen size fo proper scale
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing game thread
        drawThread = new DrawThread(getHolder(), this);
        updateThread = new UpdateThread(this);
        fps = updateThread.getFPS();

        //initializing player and touchpoints which moves them
        rocket = new Rocket(screenX, screenY, gameType);
        point = new Point(screenX/2, (int) (screenY - 1.06*rocket.getSizeY()));

        //initializing background
        background = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.gameback), BitmapFactory.decodeResource(getResources(), R.drawable.stardust), screenY);

        //initializing difficulty
        difficulty = DataStore.getDif();

        //initializing ball
        ball = new Ball(screenX, screenY, context, gameType,difficulty, Color.rgb(255, 165, 0), Color.rgb(210,105,30));
        if(difficulty == 3)  ball.changeColor(Color.argb(60, 255, 165, 0), Color.argb(80,210,105,30));

        //initializing enemy
        enemy = new Enemy(screenX, screenY, difficulty);

        //initializing music
        if(difficulty == 1)  MainMenu.setMediaPlayer(MediaPlayer.create(context.getApplicationContext(), R.raw.easytheme));
        if(difficulty == 2)  MainMenu.setMediaPlayer(MediaPlayer.create(context.getApplicationContext(), R.raw.medtheme));
        if(difficulty == 3)  MainMenu.setMediaPlayer(MediaPlayer.create(context.getApplicationContext(), R.raw.hardtheme));
        //starting mediaplayer
        if(!DataStore.isMuted())  MainMenu.getMediaPlayer().start();

        //preparing gamefield
        ball.stop();
        enemy.reset();
        isPaused = false;
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        //starting the game loop
        drawThread.setRunning(true);
        updateThread.setRunning(true);
        drawThread.start();
        updateThread.start();
    }


    public void update1()
    {
        background.update(fps);
        if(!isPaused)
        {
            //updating positions
            rocket.update(point);
            ball.update(fps);
            enemy.update(fps, ball);

            //checking collision between player1 and ball
            if(ball.getShape().top > rocket.getShape().bottom & ball.getShape().left + ball.getSizeX() > rocket.getShape().left & ball.getShape().right - ball.getSizeX() < rocket.getShape().right & ball.getShape().top < rocket.getShape().bottom + 2*ball.getSizeX())
            {
                //changing ball moving direction
                ball.playerBounce(rocket.getShape());
                ball.speedUP();
            }

            //checking collision between enemy and ball
            if(ball.getShape().bottom < enemy.getShape().bottom & ball.getShape().left + ball.getSizeX() > enemy.getShape().left & ball.getShape().right - ball.getSizeX()< enemy.getShape().right & ball.getShape().bottom + 2*ball.getSizeX() > enemy.getShape().top)
            {
                //changing ball moving direction
                ball.playerBounce(enemy.getShape());
                ball.speedUP();
            }

            //collisions between the ball and the screen
            if(ball.getShape().left < 0 || ball.getShape().right > screenX)
            {
                ball.wallBounce();
            }

            //loosing life
            if(ball.getShape().centerY() > rocket.getShape().centerY())
            {
                //resetting game field
                ball.stop();
                enemy.reset();
                //loosing point
                enemy.setPoints(enemy.getPoints()+1);
            }

            //getting point
            if(ball.getShape().centerY() < enemy.getShape().centerY())
            {
                //resetting game field
                ball.stop();
                enemy.reset();
                //getting point
                rocket.setPoints(rocket.getPoints()+1);
            }
        }
    }


    public void draw(Canvas canvas)
    {
        if(!isPaused)
        {
            super.draw(canvas);

            Paint paint = new Paint();
            paint.setColor(Color.WHITE);

            //drawing background
            background.draw(canvas);

            //drawing objects
            rocket.draw(canvas);
            enemy.draw(canvas);
            ball.draw(canvas);

            //drawing info: scores
            paint.setColor(Color.WHITE);
            paint.setTextSize(screenX/10);
            //printing landscape text on portrait canvas
            canvas.save();
            canvas.rotate(90);
            canvas.drawText(enemy.getPoints() + " : " + rocket.getPoints()  , screenY/2 - screenY/20, -screenX + screenX/10, paint);
            canvas.restore();
        }

        if(isPaused)
        {
            Paint paint = new Paint();

            //drawing background
            background.draw(canvas);
            //drawing objects
            rocket.draw(canvas);
            enemy.draw(canvas);
            ball.draw(canvas);

            //drawing info: scores
            canvas.save();
            canvas.rotate(90);
            paint.setColor(Color.WHITE);
            paint.setTextSize(screenX/10);
            canvas.drawText(enemy.getPoints() + " : " + rocket.getPoints()  , screenY/2 - screenY/20, -screenX + screenX/10, paint);

            //covering game field
            canvas.drawColor(Color.argb(200, 64, 64, 64));

            paint.setTextSize(screenX/7);
            canvas.drawText("paused"  , (float) (0.375*screenY), (float) (-0.48*screenX), paint);
            canvas.restore();
        }
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){}


    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        boolean retry = true;
        while(retry)
        {
            try
            {
                //ceaning up cache
                drawThread.setRunning(false);
                drawThread.join();
                updateThread.setRunning(false);
                updateThread.join();
                background.clear();
                ball.clear();
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
            retry = false;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
            {
                if(ball.isStopped())  ball.reset();
                resume();
            }
            case MotionEvent.ACTION_MOVE:
            {
                //moving player's rocket
                point.set((int)event.getX(), (int) (screenY - 1.06*rocket.getSizeY()));
            }
        }
        return true;
    }


    public void pause()
    {
        if(!isPaused)
        {
            isPaused = true;
            if(!DataStore.isMuted())
            {
                MainMenu.getMediaPlayer().pause();
            }
        }
    }


    public void resume()
    {
        if(isPaused)
        {
            isPaused = false;
            if(!DataStore.isMuted())
            {
                MainMenu.getMediaPlayer().start();
            }
        }
    }


    public boolean isPaused() { return isPaused; }
}
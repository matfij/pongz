package com.example.mateuszfijak.pongz;


public class UpdateThread extends Thread
{
    //fps limit
    private final int FPS = 200;

    //game type
    private int gameType;
    private SoloEngine soloEngine;
    private DuoEngine duoEngine;
    private AirEngine airEngine;
    private RankEngine rankEngine;

    //running state
    private boolean running;

    //constructors for each game type

    public UpdateThread(SoloEngine soloEngine)
    {
        super();
        this.soloEngine = soloEngine;
        this.gameType = 1;
    }

    public UpdateThread(DuoEngine duoEngine)
    {
        super();
        this.duoEngine = duoEngine;
        this.gameType = 2;
    }

    public UpdateThread(AirEngine airEngine)
    {
        super();
        this.airEngine = airEngine;
        this.gameType = 3;
    }

    public UpdateThread(RankEngine rankEngine)
    {
        super();
        this.rankEngine = rankEngine;
        this.gameType = 4;
    }


    @Override
    public void run()
    {
        //initializing fsp limiting and counters
        long startTime;
        long endTime;
        long waitTime;
        long targetTime = 1000/FPS;

        while(running)
        {
            //starting timer
            startTime = System.nanoTime();

            //updating  game field
            switch (gameType)
            {
                case 1:
                {
                    this.soloEngine.update1();
                    break;
                }

                case 2:
                {
                    this.duoEngine.update1();
                    break;
                }

                case 3:
                {
                    this.airEngine.update1();
                    break;
                }

                case 4:
                {
                    this.rankEngine.update1();
                    break;
                }
            }

            //calculating this loop time
            endTime = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - endTime;

            //if the loop was shorter then target sleep to keep static fps
            try
            {
                this.sleep(waitTime);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public void setRunning(boolean b) { running = b; }

    public int getFPS() { return FPS; }

}
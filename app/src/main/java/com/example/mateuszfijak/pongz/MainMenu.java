package com.example.mateuszfijak.pongz;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;


public class MainMenu extends Activity
{
    //indicates whether or not the app is launched first time
    private static boolean firstTimeCreated = true;

    //music player, common for most activities
    private static MediaPlayer mediaPlayer;

    //indicates if the mediaPlayer is playing
    private static boolean isPlaying;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        //making activity fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if(firstTimeCreated)
        {
            //initializing mediaPlayer
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.menutheme);
            mediaPlayer.start();
            isPlaying = true;

            firstTimeCreated = false;
        }
    }


    @Override
    public void onBackPressed()
    {
        //quiting the app
        //android.os.Process.killProcess(android.os.Process.myPid());
        //System.exit(0);
    }


    //button listeners
    public void menuClick(View view)
    {
        //depending on which button is clicked start new activity
        switch (view.getId())
        {
            case R.id.trainingButton:
            {
                //starting training game activity
                Intent intent = new Intent(getApplicationContext(), ChooseDifficulty.class);
                startActivity(intent);
                break;
            }

            case R.id.multiplayerButton:
            {
                //starting multi player game activity
                Intent intent = new Intent(getApplicationContext(), ChooseGame.class);
                startActivity(intent);
                break;
            }

            case R.id.challengeButton:
            {
                //starting challenge activity
                Intent intent = new Intent(getApplicationContext(), RankGame.class);
                startActivity(intent);
                //stopping music
                if(!DataStore.isMuted() && isPlaying)
                {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    isPlaying = false;
                }
                break;
            }

            case R.id.highscoresButton:
            {
                //starting high scores activity
                Intent intent = new Intent(getApplicationContext(), HighScores.class);
                startActivity(intent);
                //stopping music
                if(!DataStore.isMuted() && isPlaying)
                {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    isPlaying = false;
                }
                //(re)initializing high score music
                MainMenu.mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.hightheme);
                if(!DataStore.isMuted() && !isPlaying)
                {
                    MainMenu.mediaPlayer.start();
                    isPlaying = true;
                }
                break;
            }

            case R.id.settingsButton:
            {
                //starting settings activity
                Intent intent = new Intent(getApplicationContext(), Settings.class);
                startActivity(intent);
                break;
            }
        }
    }


    public static MediaPlayer getMediaPlayer() { return mediaPlayer; }

    public static boolean isIsPlaying() { return isPlaying; }

    public static void setMediaPlayer(MediaPlayer mediaPlayer) { MainMenu.mediaPlayer = mediaPlayer; }

    public static void setIsPlaying(boolean isPlaying) { MainMenu.isPlaying = isPlaying; }
}

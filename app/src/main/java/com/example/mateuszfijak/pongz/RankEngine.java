package com.example.mateuszfijak.pongz;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class RankEngine extends SurfaceView implements SurfaceHolder.Callback
{

    //executive threads
    private DrawThread drawThread;
    private UpdateThread updateThread;
    private int fps;

    //game typr and staus
    private final int gameType = 1;
    private boolean gameEnded;
    private boolean isPaused;

    //screen size for proper scalling
    private int screenX;
    private int screenY;

    //player's paddle and touchpoint
    private Point point;
    private Rocket rocket;

    //enemy object
    private Rival rival;

    //ball object
    private Ball ball;

    //game background
    private Background background;

    //application context, which allows to start new activity
    private Context context;


    public RankEngine(Context context, int screenX, int screenY)
    {
        super(context);

        //adding the callback to the surfaceholder to intercept events
        getHolder().addCallback(this);

        //makeing gamePanel focusable so it can handle events
        setFocusable(true);

        //initializing screen size fo proper scale
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing game threads
        drawThread = new DrawThread(getHolder(), this);
        updateThread = new UpdateThread(this);
        fps = updateThread.getFPS();

        //initializing player and touchpoints which moves them
        rocket = new Rocket(screenX, screenY, gameType);
        point = new Point(screenX/2, (int) (screenY - 1.06*rocket.getSizeY()));
        //initializing ball
        ball = new Ball(screenX, screenY, context, gameType,2, Color.rgb(255, 165, 0), Color.rgb(210,105,30));
        ball.stop();

        //initializing enemy
        rival = new Rival(screenX, screenY);
        rival.reset();

        //initializing bakground
        background = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.gameback), BitmapFactory.decodeResource(getResources(), R.drawable.stardust), screenY);

         //initializing music
         MainMenu.setMediaPlayer(MediaPlayer.create(context.getApplicationContext(), R.raw.ranktheme));
         if(!DataStore.isMuted())  MainMenu.getMediaPlayer().start();

        //initializing context - allows to start new activity from a SurfaceView
        this.context = context;

        //resuming game
        isPaused = false;
        gameEnded = false;
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        //starting the game loop
         drawThread.setRunning(true);
         drawThread.start();
         updateThread.setRunning(true);
         updateThread.start();
    }


    public void update1()
    {
        //updating background
        background.update(fps);

        if(!isPaused)
        {
            //updating active objects positions
            rocket.update(point);
            ball.update(fps);
            rival.update(fps, ball);

            //checking collision between player1 and ball
            if(ball.getShape().top > rocket.getShape().bottom & ball.getShape().left + ball.getSizeX() > rocket.getShape().left & ball.getShape().right - ball.getSizeX() < rocket.getShape().right & ball.getShape().top < rocket.getShape().bottom + 2*ball.getSizeX())
            {
                //changing ball moving direction
                ball.speedUP();
                ball.playerBounce(rocket.getShape());
            }

            //collision with the rival
            if(ball.getShape().bottom < rival.getShape().bottom & ball.getShape().left + ball.getSizeX() > rival.getShape().left & ball.getShape().right - ball.getSizeX()< rival.getShape().right & ball.getShape().top < rival.getShape().bottom + 2*ball.getSizeX())
            {
                //changing ball moving direction
                ball.speedUP();
                ball.playerBounce(rival.getShape());
            }

            //collisions between the ball and the sides of screen
            if(ball.getShape().left < 0 || ball.getShape().right > screenX)
            {
                ball.wallBounce();
            }

            //loosing life
            if(ball.getShape().centerY() > rocket.getShape().centerY())
            {
                //if it was last life offer the player saving scores
                if(rocket.getLives() == 1)
                {
                    DataStore.setScores(rocket.getPoints());
                    MainMenu.setIsPlaying(false);
                    gameEnded = true;
                    Intent intent = new Intent(context, SaveScores.class);
                    context.startActivity(intent);
                }
                //resetting game field
                ball.stop();
                rival.reset();
                //loosing point
                rocket.setLives(rocket.getLives()-1);
            }

            //getting point
            if(ball.getShape().centerY() < rival.getShape().centerY())
            {
                //resetting game field
                ball.stop();
                rival.reset();
                rival.upgrade();
                //getting point
                rocket.setPoints(rocket.getPoints()+1);
            }
        }
    }

    public void draw(Canvas canvas)
    {
        super.draw(canvas);

        if(!isPaused)
        {
            //initializing paint
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);

            //drawing objects
            background.draw(canvas);
            rocket.draw(canvas);
            rival.draw(canvas);
            ball.draw(canvas);

            //drawing info: scores
            paint.setColor(Color.WHITE);
            paint.setTextSize(screenX/16);
            //printing landscape text on portrait canvas
            canvas.save();
            canvas.rotate(90);
            canvas.drawText("lives : " + rocket.getLives() + "  scores: " + rocket.getPoints() , screenY/2 - screenY/8, -screenX + screenX/12, paint);
            canvas.restore();
        }

        if(isPaused)
        {
            //initializing paint
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);

            //drawing objects
            background.draw(canvas);
            rocket.draw(canvas);
            rival.draw(canvas);
            ball.draw(canvas);

            //drawing info: scores
            canvas.save();
            canvas.rotate(90);
            paint.setTextSize(screenX/16);
            canvas.drawText("lives : " + rocket.getLives() + "  scores: " + rocket.getPoints() , screenY/2 - screenY/8, -screenX + screenX/12, paint);

            //covering game field
            canvas.drawColor(Color.argb(200, 64, 64, 64));

            //drawing info: pause message
            paint.setTextSize(screenX/7);
            canvas.drawText("paused"  , (float) (0.375*screenY), (float) (-0.48*screenX), paint);
            canvas.restore();
        }
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){}


    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        boolean retry = true;
        while(retry)
        {
            try
            {
                drawThread.setRunning(false);
                drawThread.join();
                updateThread.setRunning(false);
                updateThread.join();
                background.clear();
                ball.clear();
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
            retry = false;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
            {
                if(ball.isStopped())  ball.reset();
                resume();
            }
            case MotionEvent.ACTION_MOVE:
            {
                //moving player's rocket
                point.set((int)event.getX(), (int) (screenY - 1.06*rocket.getSizeY()));
            }
        }
        return true;
    }


    public void pause()
    {
        if(!isPaused)
        {
            isPaused = true;
            if(!DataStore.isMuted())
            {
                MainMenu.getMediaPlayer().pause();
            }
        }
    }


    public void resume()
    {
        if(isPaused)
        {
            isPaused = false;
            if(!DataStore.isMuted())
            {
                MainMenu.getMediaPlayer().start();
            }
        }
    }


    public boolean isPaused() { return isPaused; }

    public boolean isGameEnded() { return gameEnded; }
}
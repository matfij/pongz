package com.example.mateuszfijak.pongz;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;


public class Settings extends Activity
{
    private Switch musicSwitch;
    private Switch soundSwitch;

    private RadioGroup colorGroup;
    private RadioButton greenBtn;
    private RadioButton blueBtn;
    private RadioButton redBtn;
    private RadioButton whiteBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //setting fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //initializing switches
        musicSwitch = findViewById(R.id.switchMusic);
        soundSwitch = findViewById(R.id.switchSound);

        //initializing radiogroup
        colorGroup = findViewById(R.id.colorGroup);
        //initializing radio buttons
        greenBtn = findViewById(R.id.radioButtonGreen);
        blueBtn = findViewById(R.id.radioButtonBlue);
        redBtn = findViewById(R.id.radioButtonRed);
        whiteBtn = findViewById(R.id.radioButtonWhite);

        //getting switches values
        if(!DataStore.isMuted())   musicSwitch.setChecked(true);
        if(DataStore.isMuted())    musicSwitch.setChecked(false);
        if(!DataStore.isSilent())  soundSwitch.setChecked(true);
        if(DataStore.isSilent())   soundSwitch.setChecked(false);

        //getting radiobuttons values
        switch (DataStore.getColorNumber())
        {
            case 1:
            {
                greenBtn.setChecked(true);
                break;
            }
            case 2:
            {
                blueBtn.setChecked(true);
                break;
            }
            case 3:
            {
                redBtn.setChecked(true);
                break;
            }
            case 4:
            {
                whiteBtn.setChecked(true);
                break;
            }
        }
    }


    @Override
    public void onBackPressed()
    {
        //saving switches vaues
        if(musicSwitch.isChecked())   DataStore.setMuted(false);
        if(!musicSwitch.isChecked())  DataStore.setMuted(true);
        if(soundSwitch.isChecked())   DataStore.setSilent(false);
        if(!soundSwitch.isChecked())  DataStore.setSilent(true);

        //saving radioButtons values
        if(greenBtn.isChecked())  DataStore.setColorNumber(1);
        if(blueBtn.isChecked())   DataStore.setColorNumber(2);
        if(redBtn.isChecked())    DataStore.setColorNumber(3);
        if(whiteBtn.isChecked())  DataStore.setColorNumber(4);

        //resuming/stopping music
        if(DataStore.isMuted() && MainMenu.isIsPlaying())
        {
            MainMenu.getMediaPlayer().pause();
            MainMenu.getMediaPlayer().release();
            MainMenu.setIsPlaying(false);
        }
        if(!DataStore.isMuted() && !MainMenu.isIsPlaying())
        {
            MainMenu.setMediaPlayer(MediaPlayer.create(getApplicationContext(), R.raw.menutheme));
            MainMenu.getMediaPlayer().start();
            MainMenu.setIsPlaying(true);
        }

        //going back to menu
        Intent intent = new Intent(getApplicationContext(), MainMenu.class);
        startActivity(intent);

        this.finish();
    }

}

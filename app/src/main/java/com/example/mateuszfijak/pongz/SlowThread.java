package com.example.mateuszfijak.pongz;

public class SlowThread extends Thread
{
    //fps limit
    private final int FPS = 50;

    //game type
    private int gameType;
    private SoloEngine soloEngine;
    private DuoEngine duoEngine;
    private AirEngine airEngine;
    private RankEngine rankEngine;

    //running state
    private boolean running;

    //constructors for each game type

    public SlowThread(SoloEngine soloEngine)
    {
        super();
        this.soloEngine = soloEngine;
        this.gameType = 1;
    }

    public SlowThread(DuoEngine duoEngine)
    {
        super();
        this.duoEngine = duoEngine;
        this.gameType = 2;
    }

    public SlowThread(AirEngine airEngine)
    {
        super();
        this.airEngine = airEngine;
        this.gameType = 3;
    }

    public SlowThread(RankEngine rankEngine)
    {
        super();
        this.rankEngine = rankEngine;
        this.gameType = 4;
    }


    @Override
    public void run()
    {
        //initializing fsp limiting and counters
        long startTime;
        long endTime;
        long waitTime;
        long targetTime = 1000/FPS;

        while(running)
        {

            //starting timer
            startTime = System.nanoTime();

            //updating  game field
            switch (gameType)
            {
                case 1:
                {
                    //not needed yet
                    break;
                }

                case 2:
                {
                    //not needed yet
                    break;
                }

                case 3:
                {
                    this.airEngine.update2();
                    break;
                }

                case 4:
                {
                    //not needed yet
                    break;
                }
            }

            //calculating this loop time
            endTime = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - endTime;

            //if the loop was shorter then target sleep to keep static fps
            try
            {
                this.sleep(waitTime);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }


    public void setRunning(boolean b) { running = b; }

    public int getFPS() { return FPS; }
}

package com.example.mateuszfijak.pongz;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;


public class HighScores extends Activity
{
    private static boolean firstTimeCreated = true;
    private static boolean isPlaying;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //setting fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);

        if(firstTimeCreated)
        {
            //handling music stuff
            firstTimeCreated = false;
            isPlaying = false;

            //if its the first time, create that file

            //initializing output data objects
            FileOutputStream fileOutputStream = null;

            //trying to create file in case it was first game after instlation
            try
            {
                fileOutputStream = openFileOutput(DataStore.getFileName(), MODE_APPEND);
                fileOutputStream.write("".getBytes());
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            finally
            {
                try
                {
                    fileOutputStream.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

        //reading highscores from text file
        //initializing file reading objects
        FileInputStream fileInputStream = null;
        //trying to open file
        try
        {
            fileInputStream = openFileInput(DataStore.getFileName());
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            //data containers
            String names[] = new String[99];
            int[] scores = new int[99];
            String temp;
            //iterators
            int iterator = 1;
            int nameCount = 0;
            int scoreCount = 0;

            //saving data to point and name arrays
            while((temp = bufferedReader.readLine()) != null)
            {
                if(iterator%2 == 0)
                {
                    scores[scoreCount] = Integer.parseInt(temp);
                    scoreCount++;
                }
                else
                {
                    names[nameCount] = temp;
                    nameCount++;
                }
                iterator++;
            }

            //sorting high scores
            int n = scoreCount;
            int k;
            for (int m = n; m >= 0; m--)
            {
                for (int i = 0; i < n - 1; i++)
                {
                    k = i + 1;
                    if (-scores[i] > -scores[k])
                    {
                        int tempInt;
                        String tempString;

                        tempInt = scores[i];
                        tempString = names[i];

                        scores[i] = scores[k];
                        names[i] = names[k];

                        scores[k] = tempInt;
                        names[k] = tempString;
                    }
                }
            }


            //displaying high scores if they are not empty
            //the champion
            TextView champView = findViewById(R.id.championView);
            if(names[0]!= null)  champView.setText(names[0] + "   " +scores[0] + "\n");
            //the rest
            TextView scoreView = findViewById(R.id.scoreView);
            if(names[1]!= null)  scoreView.setText(names[1] + "   " +scores[1] + "\n");   else   scoreView.setText("\n");
            if(names[2]!= null)  scoreView.append(names[2] + "   " +scores[2] + "\n");    else   scoreView.append("\n");
            if(names[3]!= null)  scoreView.append(names[3] + "   " +scores[3] + "\n");    else   scoreView.append("\n");
            if(names[4]!= null)  scoreView.append(names[4] + "   " +scores[4] + "\n");    else   scoreView.append("\n");
            if(names[5]!= null)  scoreView.append(names[5] + "   " +scores[5] + "\n");    else   scoreView.append("\n");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                fileInputStream.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Runtime.getRuntime().gc();
    }


    @Override
    public void onBackPressed()
    {
        //going back to menu
        Intent intent = new Intent(getApplicationContext(), MainMenu.class);
        startActivity(intent);
        //stopping music
        MainMenu.getMediaPlayer().release();
        MainMenu.setIsPlaying(false);
        isPlaying = false;
        //(re)initializing music
        if(!DataStore.isMuted())
        {
            MainMenu.setMediaPlayer(MediaPlayer.create(getApplicationContext(), R.raw.menutheme));
            MainMenu.getMediaPlayer().start();
            MainMenu.setIsPlaying(true);
        }
    }


    public void  click(View view)
    {
        switch (view.getId())
        {
            case R.id.backButton:
            {
                //going back to menu
                Intent intent = new Intent(getApplicationContext(), MainMenu.class);
                startActivity(intent);
                //stopping music
                MainMenu.getMediaPlayer().release();
                MainMenu.setIsPlaying(false);
                isPlaying = false;
                //resuming menu music
                if(!DataStore.isMuted())
                {
                    MainMenu.setMediaPlayer(MediaPlayer.create(getApplicationContext(), R.raw.menutheme));
                    MainMenu.getMediaPlayer().start();
                    MainMenu.setIsPlaying(true);
                }
                break;
            }

            case R.id.resButton:
            {
                //going to confirm section
                Intent intent = new Intent(getApplicationContext(), ConfirmDelete.class);
                startActivity(intent);
                break;
            }
        }
    }
}

package com.example.mateuszfijak.pongz;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;


public class Rival
{
    //rival body
    private RectF shape;
    private float sizeX;
    private float sizeY;
    private int color;
    private int borderColor;

    //rival progression
    private int level;
    private double speed;
    private double vision;
    private double speed2;

    //screen size for proper scalling
    private int screenX;
    private int screenY;


    public Rival(int screenX, int screenY)
    {
        //initializing screen size
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing body
        shape = new RectF();
        this.sizeX = screenX/5;
        this.sizeY = screenY/40;

        //initial difficulty level
        this.level = 1;
        this.speed = 0.7*screenX;
        this.vision = 0.45*screenY;
    }


    public void reset()
    {
        //resetting rival position to the center
        shape.left = (int) (screenX/2 - sizeX/2);
        shape.top = (int) (0.013*screenY);
        shape.right = (int) (screenX/2 + sizeX/2);
        shape.bottom = (int)(shape.top + sizeY);
    }


    //updating enemy position
    public void update(double fps, Ball ball)
    {
        if(level > 6)  ball.setDifficulty(3);

        if(level >10)  ball.changeColor(Color.argb(40, 255, 165, 0), Color.argb(80,210,105,30));

        if(level <= 3)
        {
            //changing color
            this.color = Color.rgb(75,0,130);
            this.borderColor = Color.rgb(192,192,192);
        }

        if(3 < level && level <= 6)
        {
            //changing color
            this.color = Color.rgb(138,43,226);
            this.borderColor = Color.rgb(192,192,192);
        }

        if(6 < level && level <= 10)
        {
            //changing color
            this.color = Color.rgb(255,0,255);
            this.borderColor = Color.rgb(192,192,192);
        }

        if(10 >= level)
        {
            if (ball.getShape().centerX() > this.shape.centerX() && Math.abs(ball.getAng()) > 100*Math.PI/180 && ball.getShape().centerY() < vision)
            {
                shape.left = (int) (shape.left + speed / fps);
                shape.top = (int) (0.012*screenY);
                shape.right = (int) (shape.left + sizeX);
                shape.bottom = (int)(shape.top + sizeY);
            }

            if (ball.getShape().centerX() < this.shape.centerX() && Math.abs(ball.getAng()) > 100*Math.PI/180 && ball.getShape().centerY() < vision)
            {
                shape.left = (int) (shape.left - speed / fps);
                shape.top = (int) (0.012*screenY);
                shape.right = (int) (shape.left + sizeX);
                shape.bottom = (int)(shape.top + sizeY);
            }
        }

        if(10 < level)
        {
            //resizing and repainting enemy
            this.sizeX = screenX/8;
            this.color = Color.rgb(248,248,255);
            this.borderColor = Color.rgb(192,192,192);

            //upgrading speed calculating system
            this.speed = 0.25*ball.getVel() + 0.67*speed2;

            //upgrading tracking system
            if (ball.getShape().centerX() > this.shape.centerX())
            {
                shape.left = (int) (shape.left + speed / fps);
                shape.top = (int) (0.012*screenY);
                shape.right = (int) (shape.left + sizeX);
                shape.bottom = (int)(shape.top + sizeY);
            }
            if (ball.getShape().centerX() < this.shape.centerX())
            {
                shape.left = (int) (shape.left - speed / fps);
                shape.top = (int) (0.012*screenY);
                shape.right = (int) (shape.left + sizeX);
                shape.bottom = (int)(shape.top + sizeY);
            }
        }
    }


    //drawing enemy
    public void draw(Canvas canvas)
    {
        //initializing ball colorss+
        Paint paint1 = new Paint();
        paint1.setColor(borderColor);
        Paint paint2 = new Paint();
        paint2.setColor(color);

        //drawing edges
        RectF tempRect = new RectF(shape.left + sizeX/10, shape.top + sizeY/10, shape.right - sizeX/10, shape.bottom - sizeY/10);

        //drawing rects as ovals
        canvas.drawOval(shape, paint1);
        canvas.drawOval(tempRect, paint2);
    }


    public void upgrade()
    {
        //improving enemy skill level
        this.level++;
        this.speed += 0.05*this.screenX;
        this.speed2 = this.speed;
        this.vision += 0.07*screenX;
    }


    public RectF getShape() {
        return shape;
    }
}

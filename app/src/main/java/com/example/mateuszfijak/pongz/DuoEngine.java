package com.example.mateuszfijak.pongz;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class DuoEngine extends SurfaceView implements SurfaceHolder.Callback
{
    //executive threads
    private DrawThread drawThread;
    private UpdateThread updateThread;
    private int fps;

    //type of the game which indicates game rules
    private final int gameType = 1;
    //game status
    private boolean isPaused;
    private boolean redWon = false;
    private boolean blueWon = false;

    //players paddles and touchpoints
    private Point point1;
    private Point point2;
    private Rocket rocket1;
    private Rocket rocket2;

    //ball object
    private Ball ball;

    //game background
    private Background background;

    //screen size for proper scalling
    private int screenX;
    private int screenY;


    public DuoEngine(Context context, int screenX, int screenY)
    {
        super(context);

        //initializing game thread
        drawThread = new DrawThread(getHolder(), this);
        updateThread = new UpdateThread(this);
        fps = updateThread.getFPS();

        //adding the callback to the surfaceholder to intercept events
        getHolder().addCallback(this);

        //makeing gamePanel focusable so it can handle events
        setFocusable(true);

        //initializing screen size fo proper scale
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing player and touchpoints which moves them
        rocket1 = new Rocket(screenX, screenY, gameType, Color.rgb(100,149,237), Color.rgb(135,206,250));
        rocket2 = new Rocket(screenX, screenY, gameType, Color.rgb(220,25,55), Color.rgb(255,49,0));
        point1 = new Point(screenX/2,(int)(screenY - 1.06*rocket1.getSizeY()));
        point2 = new Point(screenX/2,(int)(1.06*rocket2.getSizeY()));

        //initializing ball
        ball = new Ball(screenX, screenY, context, gameType,2, Color.rgb(255, 165, 0), Color.rgb(210,105,30));
        ball.stop();

        //initializing music
        MainMenu.setMediaPlayer(MediaPlayer.create(context.getApplicationContext(), R.raw.duotheme));
        if(!DataStore.isMuted())  MainMenu.getMediaPlayer().start();

        //initializing game background
        background = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.gameback), BitmapFactory.decodeResource(getResources(), R.drawable.stardust), screenY);

        //resuming game
        isPaused = false;
        redWon = false;
        blueWon = false;
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        //starting the game loop
       drawThread.setRunning(true);
       drawThread.start();
       updateThread.setRunning(true);
       updateThread.start();
    }


    public void update1()
    {
        //updating background
        background.update(fps);

        if(!isPaused)
        {
            //updating positions
            rocket1.update(point1);
            rocket2.update(point2);
            ball.update(fps);

            //checking collision between player1 and ball
            if(ball.getShape().top > rocket1.getShape().bottom & ball.getShape().left + ball.getSizeX() > rocket1.getShape().left & ball.getShape().right - ball.getSizeX() < rocket1.getShape().right & ball.getShape().top < rocket1.getShape().bottom + 2*ball.getSizeX())
            {
                //changing ball moving direction
                ball.playerBounce(rocket1.getShape());
                ball.speedUP();
            }
            //checking collision between player2 and ball
            else if(ball.getShape().bottom < rocket2.getShape().top & ball.getShape().left + ball.getSizeX() > rocket2.getShape().left & ball.getShape().right - ball.getSizeX() < rocket2.getShape().right & ball.getShape().bottom + 2*ball.getSizeX() > rocket2.getShape().top)
            {
                //changing ball moving direction
                ball.playerBounce(rocket2.getShape());
                ball.speedUP();
            }

            //collisions between the ball and the screen
            if(ball.getShape().left < 0 || ball.getShape().right > screenX)
            {
                ball.wallBounce();
            }

            //ending set
            if(ball.getShape().centerY() > rocket1.getShape().centerY())
            {
                ball.stop();

                //if player got 5 scores, finish the game
                if(rocket1.getPoints() == 4)
                {
                    redWon = true;
                }
                rocket1.setPoints(rocket1.getPoints()+1);
            }
            if(ball.getShape().centerY() < rocket2.getShape().centerY())
            {
                ball.stop();

                //if player got 5 scores, finish the game
                if(rocket2.getPoints() == 4)
                {
                    blueWon = true;
                }
                rocket2.setPoints(rocket2.getPoints()+1);
            }
        }
    }


    public void draw(Canvas canvas)
    {
        if(!isPaused)
        {
            super.draw(canvas);

            //initializing paint
            Paint paint = new Paint();

            //drawing objects
            background.draw(canvas);
            rocket1.draw(canvas);
            rocket2.draw(canvas);
            ball.draw(canvas);

            //drawing info: scores
            canvas.save();
            canvas.rotate(90);
            paint.setTextSize(screenX/10);
            paint.setColor(Color.WHITE);
            canvas.drawText(rocket1.getPoints() + " : " + rocket2.getPoints(), screenY/2 - screenY/20, -screenX + screenX/10, paint);
            canvas.restore();

            //drawing endgame info
            if(redWon || blueWon)
            {
                //covering game field
                canvas.drawColor(Color.argb(200, 64, 64, 64));

                if(redWon)
                {
                    paint.setColor(Color.RED);
                    paint.setTextSize(screenX/9);
                    canvas.save();
                    canvas.rotate(90);
                    canvas.drawText("Red Player Wins!", screenY/2 - screenY/4, (float) (-0.6*screenX), paint);
                    paint.setTextSize(screenX/13);
                    canvas.drawText("tap to continue...", screenY/3, (float) (-0.36*screenX), paint);
                    canvas.restore();
                }
                if(blueWon)
                {
                    paint.setColor(Color.BLUE);
                    paint.setTextSize(screenX/9);
                    canvas.save();
                    canvas.rotate(90);
                    canvas.drawText("Blue Player Wins!", (float) (screenY/2 - 0.25*screenY), (float) (-0.6*screenX), paint);
                    paint.setTextSize(screenX/13);
                    canvas.drawText("tap to continue...", (float) (screenY/3), (float) (-0.36*screenX), paint);
                    canvas.restore();
                }
            }
        }

        if(isPaused)
        {
            //initializing paint
            Paint paint = new Paint();

            //drawing objects
            background.draw(canvas);
            rocket1.draw(canvas);
            rocket2.draw(canvas);
            ball.draw(canvas);

            //drawing info: scores
            canvas.save();
            canvas.rotate(90);
            paint.setTextSize(screenX/10);
            paint.setColor(Color.WHITE);
            canvas.drawText(rocket1.getPoints() + " : " + rocket2.getPoints(), screenY/2 - screenY/20, -screenX + screenX/10, paint);
            canvas.restore();

            //covering game field
            canvas.drawColor(Color.argb(200, 64, 64, 64));

            //drawing pause info
            canvas.save();
            canvas.rotate(90);
            paint.setColor(Color.WHITE);
            paint.setTextSize(screenX/7);
            canvas.drawText("paused"  , (float) (0.375*screenY), (float) (-0.48*screenX), paint);
            canvas.restore();
        }

    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){}


    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        //ending threads life, cleaning up cache
        boolean retry = true;
        while(retry)
        {
            try
            {
                drawThread.setRunning(false);
                drawThread.join();
                updateThread.setRunning(false);
                updateThread.join();
                background.clear();
                ball.clear();
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
            retry = false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        //touch positions
        int x, y;
        //number of touched areas
        int num = event.getPointerCount();
        //masked touch action
        int action = event.getActionMasked();

        if(1 < num)
        {
            for (int a = 0; a < num; a++)
            {
                try
                {
                    //getting touch position
                    x = (int) event.getX(event.findPointerIndex(a));
                    y = (int) event.getY(event.findPointerIndex(a));

                    switch (action)
                    {
                        case MotionEvent.ACTION_DOWN:
                        {
                            if(ball.isStopped())  ball.reset2();
                            resume();
                        }
                        case  MotionEvent.ACTION_MOVE:
                        {
                            //moving players
                            if(y > screenY/2)  point1.set(x, (int) (screenY - 1.06*rocket1.getSizeY()));
                            if(y < screenY/2)  point2.set(x, (int) (1.06*rocket2.getSizeY()));
                        }
                    }
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        else
        {
            //getting touch position
            x = (int) event.getX();
            y = (int) event.getY();

            switch (action)
            {
                case MotionEvent.ACTION_DOWN:
                {
                    if(ball.isStopped())  ball.reset2();
                    resume();
                }
                case  MotionEvent.ACTION_MOVE:
                {
                    //moving players
                    if(y > screenY/2)  point1.set(x, (int) (screenY - 1.06*rocket1.getSizeY()));
                    if(y < screenY/2)  point2.set(x, (int) (1.06*rocket2.getSizeY()));
                }
            }
        }
        return true;
    }


    public void pause()
    {
        if(!isPaused && !blueWon && !redWon)
        {
            isPaused = true;
            if(!DataStore.isMuted())
            {
                MainMenu.getMediaPlayer().pause();
            }
        }
    }


    public void resume()
    {
        if(isPaused)
        {
            if(!DataStore.isMuted())
            {
                //resuming music
                MainMenu.getMediaPlayer().start();
            }
            isPaused = false;
        }

        if(blueWon || redWon)
        {
            //resetting game field
            rocket1.setPoints(0);
            rocket2.setPoints(0);
            blueWon = false;
            redWon = false;
            ball.reset();
            ball.stop();
        }
    }


    public boolean isPaused() { return isPaused; }

    public boolean isRedWon() { return redWon; }

    public boolean isBlueWon() { return blueWon; }
}
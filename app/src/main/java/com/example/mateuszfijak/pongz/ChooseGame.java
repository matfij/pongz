package com.example.mateuszfijak.pongz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

public class ChooseGame extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_game);

        //setting fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }


    @Override
    protected void onPause()
    {
        super.onPause();
        //cleaning up memory
        this.finish();
    }


    @Override
    public void onBackPressed()
    {
        //going back to menu
        Intent intent = new Intent(getApplicationContext(), MainMenu.class);
        startActivity(intent);
    }


    public void menuClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imageButton1:
            {
                //starting game
                Intent intent = new Intent(getApplicationContext(), DuoGame.class);
                startActivity(intent);
                //stopping menu music
                if(!DataStore.isMuted())
                {
                    MainMenu.getMediaPlayer().stop();
                    MainMenu.getMediaPlayer().release();
                }
                break;
            }
            case R.id.imageButton2:
            {
                //starting game
                Intent intent = new Intent(getApplicationContext(), AirGame.class);
                startActivity(intent);
                //stopping menu music
                if(!DataStore.isMuted())
                {
                    MainMenu.getMediaPlayer().stop();
                    MainMenu.getMediaPlayer().release();
                }
                break;
            }
        }
    }
}

package com.example.mateuszfijak.pongz;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.FileOutputStream;
import java.io.IOException;


public class SaveScores extends Activity
{
    //saving data stuff
    private int scores;
    private String name;
    private String points;
    private EditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_scores);

        //setting fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //initializing player scores
        scores = DataStore.getScores();

        //displaying player score
        TextView textView = (TextView) findViewById(R.id.scoreText);
        textView.setText("Your scores: " + scores);

        //initializing editText
        editText = findViewById(R.id.nameText);

        //stoping mediaplayer
        MainMenu.getMediaPlayer().pause();
        MainMenu.getMediaPlayer().release();


    }


    @Override
    public void onPause()
    {
        onBackPressed();
        super.onPause();
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Runtime.getRuntime().gc();
    }


    @Override
    public void onBackPressed()
    {
        //quiting to main menu
        Intent intent = new Intent(getApplicationContext(), MainMenu.class);
        startActivity(intent);
        //(re)initializing music
        if(!DataStore.isMuted())
        {
            MainMenu.getMediaPlayer().release();
            MainMenu.setMediaPlayer(MediaPlayer.create(getApplicationContext(), R.raw.menutheme));
            MainMenu.getMediaPlayer().start();
            MainMenu.setIsPlaying(true);
        }
    }


    //buttons listeners
    public void  click(View view)
    {
        switch (view.getId())
        {
            case R.id.saveButton:
            {
                //saving highscores data
                //getting player name
                name = editText.getText().toString();
                if(name.isEmpty()) name = "alias";
                points = String.valueOf(scores);
                //initializing output data objects
                FileOutputStream fileOutputStream = null;

                //trying to open file
                try
                {
                    fileOutputStream = openFileOutput(DataStore.getFileName(), MODE_APPEND);
                    fileOutputStream.write(name.getBytes());
                    fileOutputStream.write('\n');
                    fileOutputStream.write(points.getBytes());
                    fileOutputStream.write('\n');

                    //displaying info message
                    Toast.makeText(this, "HighScores saved!", Toast.LENGTH_LONG).show();
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                finally
                {
                    try
                    {
                        fileOutputStream.close();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                //opening higscores activity
                Intent intent = new Intent(getApplicationContext(), HighScores.class);
                startActivity(intent);

                //(re)initializing music
                if(!DataStore.isMuted())
                {
                    MainMenu.setMediaPlayer(MediaPlayer.create(getApplicationContext(), R.raw.hightheme));
                    MainMenu.getMediaPlayer().start();
                }
                break;
            }

            case R.id.cancelButton:
            {
                //quiting to main menu
                Intent intent = new Intent(getApplicationContext(), MainMenu.class);
                startActivity(intent);
                //(re)initializing music
                if(!DataStore.isMuted())
                {
                    MainMenu.getMediaPlayer().release();
                    MainMenu.setMediaPlayer(MediaPlayer.create(getApplicationContext(), R.raw.menutheme));
                    MainMenu.getMediaPlayer().start();
                    MainMenu.setIsPlaying(true);
                }
                break;
            }
        }
    }
}

package com.example.mateuszfijak.pongz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;


public class AirGame extends Activity
{

    private AirEngine airEngine;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tennis);

        //set to full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //getting screen resolution
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        //initializing engine
        airEngine =  new AirEngine(this, size.x, size.y);

        //starting the game
        setContentView(airEngine);
    }


    @Override
    protected void onPause()
    {
        super.onPause();

        //going back to menu
        Intent intent = new Intent(getApplicationContext(), MainMenu.class);
        startActivity(intent);
        //resuming menu music
        if(!DataStore.isMuted())
        {
            MainMenu.getMediaPlayer().pause();
            MainMenu.getMediaPlayer().release();
            MainMenu.setMediaPlayer(MediaPlayer.create(getApplicationContext(), R.raw.menutheme));
            MainMenu.getMediaPlayer().start();
            MainMenu.setIsPlaying(true);
        }
    }


    @Override
    protected void onResume()
    {
        //resuming game
        airEngine.resume();
        super.onResume();
    }


    @Override
    public void onBackPressed()
    {
        if(airEngine.isPaused() || airEngine.isBlueWon() || airEngine.isRedWon())
        {
            //going back to menu
            Intent intent = new Intent(getApplicationContext(), MainMenu.class);
            startActivity(intent);
            //resuming menu music
            if(!DataStore.isMuted())
            {
                MainMenu.getMediaPlayer().pause();
                MainMenu.getMediaPlayer().release();
                MainMenu.setMediaPlayer(MediaPlayer.create(getApplicationContext(), R.raw.menutheme));
                MainMenu.getMediaPlayer().start();
                MainMenu.setIsPlaying(true);
            }
            this.finish();
        }
        airEngine.pause();
    }
}

package com.example.mateuszfijak.pongz;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.VelocityTracker;

import java.util.ArrayList;
import java.util.Random;


public class AirEngine extends SurfaceView implements SurfaceHolder.Callback
{

    Random rand;
    ArrayList<String> strings;

    //threads of execution
    private DrawThread gameThread;
    private UpdateThread updateThread;
    private SlowThread slowThread;
    private int fps1;
    private int fps2;

    //type of the game which indicates game rules
    private final int gameType = 2;
    //game status
    private boolean isPaused;
    private boolean redWon = false;
    private boolean blueWon = false;

    //players rockets, touchpoints(making players capable od moving rockets)
    private Rocket rocket1;
    private Rocket rocket2;
    private Point point1;
    private Point point2;

    //objects which allows to track swipe velocity and adjust how strong the hit between rocket and ball was
    private VelocityTracker velocityTracker1 = null;
    private VelocityTracker velocityTracker2 = null;
    private double power1;
    private double power2;

    //ball object
    private Ball ball;

    //screen size for proper scaling
    private int screenX;
    private int screenY;

    //game field apperance
    private Background background;
    //game field bounds
    RectF topLeft;
    RectF topRight;
    RectF bottomLeft;
    RectF bottomRight;
    RectF sideLeft;
    RectF sideRight;


    public AirEngine(Context context, int screenX, int screenY)
    {
        super(context);

        //adding the callback to the surfaceholder to intercept events
        getHolder().addCallback(this);
        //making gamePanel focusable so it can handle events
        setFocusable(true);

        //initializing game threads
        gameThread = new DrawThread(getHolder(), this);
        updateThread = new UpdateThread(this);
        slowThread = new SlowThread(this);
        fps1 = updateThread.getFPS();
        fps2 = slowThread.getFPS();

        //initializing screen size fo proper scaling
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing player and touchpoints which moves them
        rocket1 = new Rocket(screenX, screenY, gameType, Color.rgb(100,149,237), Color.rgb(135,206,250));
        rocket2 = new Rocket(screenX, screenY, gameType, Color.rgb(220,25,55), Color.rgb(255,49,0));
        point1 = new Point(screenX/2,screenY - screenY/10);
        point2 = new Point(screenX/2,screenY/10);

        //initializing and setting ball
        ball = new Ball(screenX, screenY, context, gameType, 1,  Color.rgb(	240, 255, 255), Color.WHITE);
        ball.stop();

        //initializing music
        MainMenu.setMediaPlayer(MediaPlayer.create(context.getApplicationContext(), R.raw.duotheme));
        if(!DataStore.isMuted())  MainMenu.getMediaPlayer().start();

        //initializing game field
        background = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.gameback), BitmapFactory.decodeResource(getResources(), R.drawable.stardust), screenY);
        //initializing side lines
        topLeft = new RectF(screenX/30,screenY/75,screenX/3, screenY/50);
        topRight = new RectF(2*screenX/3,screenY/75,screenX - screenX/30, screenY/50);
        bottomLeft = new RectF(screenX/30,screenY - screenY/50,screenX/3, screenY - screenY/75);
        bottomRight = new RectF(2*screenX/3,screenY - screenY/50,screenX - screenX/30, screenY - screenY/75);
        sideLeft = new RectF(screenX/50,screenY/75,screenX/30, screenY - screenY/75);
        sideRight = new RectF(screenX - screenX/30,screenY/75,screenX - screenX/50, screenY - screenY/75);

        //resuming game
        isPaused = false;
        redWon = false;
        blueWon = false;
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        //starting the game loop
        gameThread.setRunning(true);
        updateThread.setRunning(true);
        slowThread.setRunning(true);
        gameThread.start();
        updateThread.start();
        slowThread.start();
    }


    //higher frequency update method
    public void update1()
    {
        //updating background
        background.update(fps1);

        if(!isPaused)
        {
            //updating positions of active objects;
            rocket1.update(point1);
            rocket2.update(point2);

            //if ball velocity is big enough, update it with higher fps ratio
            if(ball.getVel() > screenX)
            {
                //updating ball position
                ball.update(fps1);

                //collisions between the ball and the bounding lines
                if(ball.getShape().left < sideLeft.right || ball.getShape().right > sideRight.left)
                {
                    ball.wallBounce();
                }
                if((ball.getShape().bottom < topLeft.top || ball.getShape().top < topRight.bottom) && (ball.getShape().left < topLeft.right || ball.getShape().right > topRight.left))
                {
                    ball.wallBounce2();
                }
                if((ball.getShape().top > bottomLeft.bottom || ball.getShape().bottom > bottomRight.bottom) && (ball.getShape().left < topLeft.right || ball.getShape().right > topRight.left))
                {
                    ball.wallBounce2();
                }
            }

            //collisions between the ball and the bounding lines
            if(ball.getShape().left + ball.getSizeX()/4 < sideLeft.right || ball.getShape().right > sideRight.left)
            {
                ball.wallBounce();
            }
            if((ball.getShape().bottom < topLeft.top || ball.getShape().top < topRight.bottom) && (ball.getShape().left < topLeft.right || ball.getShape().right > topRight.left))
            {
                ball.wallBounce2();
            }
            if((ball.getShape().top > bottomLeft.bottom || ball.getShape().bottom > bottomRight.bottom) && (ball.getShape().left < topLeft.right || ball.getShape().right > topRight.left))
            {
                ball.wallBounce2();
            }

            //checking collision between player1 and ball
            if(rocket1.collide(ball))
            {
                //changing ball moving direction
                ball.playerBounce(rocket1.getShape());
                ball.speedUP2(power1);
            }
            //checking collision between player2 and ball
            else if(rocket2.collide(ball))
            {
                //changing ball moving direction
                ball.playerBounce(rocket2.getShape());
                ball.speedUP2(power2);
            }

            //ending set
            if(ball.getShape().top > bottomLeft.bottom && ball.getShape().centerX() > topLeft.right && ball.getShape().centerX() < topRight.left)
            {
                //if player got 5 scores, finish the game
                if(rocket1.getPoints() == 4)
                {
                    rocket1.setPoints(rocket1.getPoints()+1);
                    redWon = true;
                    ball.stop();
                }
                else
                {
                    ball.stop2();
                    rocket1.setPoints(rocket1.getPoints()+1);
                }
            }

            if(ball.getShape().bottom < topLeft.top && ball.getShape().centerX() > topLeft.right && ball.getShape().centerX() < topRight.left)
            {
                //if player got 5 scores, finish the game
                if(rocket2.getPoints() == 4)
                {
                    rocket2.setPoints(rocket2.getPoints()+1);
                    blueWon = true;
                    ball.stop();
                }
                else
                {
                    ball.stop2();
                    rocket2.setPoints(rocket2.getPoints()+1);
                }
            }
        }
    }


    //lower frequency update method
    public void update2()
    {
        //if ball velocity is big enough, update it with higher fps
        if(!isPaused && ball.getVel() <= screenX)
        {
            //updating ball position
            ball.update(fps2);

            //collisions between the ball and the bounding lines
            if(ball.getShape().left < sideLeft.right || ball.getShape().right > sideRight.left)
            {
                ball.wallBounce();
            }
            if((ball.getShape().bottom < topLeft.top || ball.getShape().top < topRight.bottom) && (ball.getShape().left < topLeft.right || ball.getShape().right > topRight.left))
            {
                ball.wallBounce2();
            }
            if((ball.getShape().top > bottomLeft.bottom || ball.getShape().bottom > bottomRight.bottom) && (ball.getShape().left < topLeft.right || ball.getShape().right > topRight.left))
            {
                ball.wallBounce2();
            }
        }
    }


    public void draw(Canvas canvas)
    {
        if(!isPaused)
        {
            super.draw(canvas);

            //initializing paint
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);

            //drawing gamefield
            background.draw(canvas);
            canvas.drawRect(topLeft, paint);
            canvas.drawRect(topRight, paint);
            canvas.drawRect(bottomLeft, paint);
            canvas.drawRect(bottomRight, paint);
            canvas.drawRect(sideLeft, paint);
            canvas.drawRect(sideRight, paint);

            //drawing active objects
            rocket1.draw(canvas);
            rocket2.draw(canvas);
            ball.draw(canvas);

            //drawing info: scores
            paint.setTextSize(screenX/10);
            canvas.save();
            canvas.rotate(90);
            canvas.drawText(rocket1.getPoints() + " : " + rocket2.getPoints(), screenY/2 - screenY/16, -screenX + screenX/8, paint);
            canvas.restore();

            //drawing endgame info
            if(redWon || blueWon)
            {
                //covering game field
                canvas.drawColor(Color.argb(200, 64, 64, 64));

                if (redWon)
                {
                    paint.setColor(Color.RED);
                    canvas.save();
                    canvas.rotate(90);
                    paint.setTextSize(screenX / 9);
                    canvas.drawText("Red Player Wins!", screenY / 2 - screenY / 4, (float) (-0.6 * screenX), paint);
                    paint.setTextSize(screenX / 13);
                    canvas.drawText("tap to continue...", screenY / 3 , (float) (-0.36 * screenX), paint);
                    canvas.restore();
                }
                if (blueWon)
                {
                    paint.setColor(Color.BLUE);
                    canvas.save();
                    canvas.rotate(90);
                    paint.setTextSize(screenX / 9);
                    canvas.drawText("Blue Player Wins!", (float) (screenY / 2 - 0.25 * screenY), (float) (-0.6 * screenX), paint);
                    paint.setTextSize(screenX / 13);
                    canvas.drawText("tap to continue...", (float) (screenY / 3 ), (float) (-0.36 * screenX), paint);
                    canvas.restore();
                }
            }
        }

        if(isPaused)
        {
            //initializing paint
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);

            //drawing game field
            background.draw(canvas);
            canvas.drawRect(topLeft, paint);
            canvas.drawRect(topRight, paint);
            canvas.drawRect(bottomLeft, paint);
            canvas.drawRect(bottomRight, paint);
            canvas.drawRect(sideLeft, paint);
            canvas.drawRect(sideRight, paint);

            //drawing active objects
            rocket1.draw(canvas);
            rocket2.draw(canvas);
            ball.draw(canvas);

            //drawing info: scores
            canvas.save();
            canvas.rotate(90);
            paint.setTextSize(screenX/10);
            canvas.drawText(rocket1.getPoints() + " : " + rocket2.getPoints(), screenY/2 - screenY/16, -screenX + screenX/8, paint);

            //covering game field
            canvas.drawColor(Color.argb(200, 64, 64, 64));

            //drawing info: pause status
            paint.setTextSize(screenX/7);
            canvas.drawText("paused"  , (float) (0.375*screenY), (float) (-0.48*screenX), paint);
            canvas.restore();
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){}


    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        //ending threads life, cleaning up cache
        boolean retry = true;
        while(retry)
        {
            try
            {
                gameThread.setRunning(false);
                gameThread.join();
                updateThread.setRunning(false);
                updateThread.join();
                slowThread.setRunning(false);
                slowThread.join();
                background.clear();
                ball.clear();
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
            retry = false;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        //touch positions
        int x, y;
        //number of touched areas
        int num = event.getPointerCount();
        //masked touch action
        int action = event.getActionMasked();

        //HANDLING MULTITOUCH GESTURES
        if (1 < num)
        {
            //looping as many times as many fingers touching screen
            for (int a = 0; a < num; a++)
            {
                switch (action)
                {
                    case MotionEvent.ACTION_DOWN:
                    {
                        resume();

                        //initializing a new VelocityTracker object to watch the velocity of a motion
                        if (velocityTracker1 == null)  velocityTracker1 = VelocityTracker.obtain();
                        else  velocityTracker1.clear();
                        if (velocityTracker2 == null)  velocityTracker2 = VelocityTracker.obtain();
                        else  velocityTracker2.clear();

                        //adding a user's movement to the tracker.
                        velocityTracker1.addMovement(event);
                        velocityTracker2.addMovement(event);
                        try
                        {
                            //getting pointers cords
                            x = (int) event.getX(event.findPointerIndex(a));
                            y = (int) event.getY(event.findPointerIndex(a));

                            //resetting hit powers
                            if (y > 0.5 * screenY)
                            {
                                power1 = 0;
                            }
                            if (y < 0.5 * screenY)
                            {
                                power2 = 0;
                            }
                        }
                        catch(Exception ex)
                        {
                            ex.printStackTrace();
                        }
                        break;
                    }

                    case MotionEvent.ACTION_MOVE:
                    {
                        try
                        {
                            //getting pointers cords
                            x = (int) event.getX(event.findPointerIndex(a));
                            y = (int) event.getY(event.findPointerIndex(a));

                            //moving players
                            if (y > 0.5 * screenY)
                            {
                                //setting position
                                point1.set(x, y);
                                //calculating players speed
                                velocityTracker1.addMovement(event);
                                velocityTracker1.computeCurrentVelocity(1000);
                                //setting speed/power
                              //  velocityTracker1 = VelocityTracker.obtain();
                                power1 = Math.abs(2 * velocityTracker1.getXVelocity() + Math.abs(3 * velocityTracker1.getYVelocity()));
                            }
                            if (y < 0.5 * screenY)
                            {
                                //setting position
                                point2.set(x, y);
                                //calculating players speed
                              //  velocityTracker2 = VelocityTracker.obtain();
                                velocityTracker2.addMovement(event);
                                velocityTracker2.computeCurrentVelocity(1000);
                                //setting speed/power
                                power2 = Math.abs(2 * velocityTracker2.getXVelocity() + Math.abs(3 * velocityTracker2.getYVelocity()));
                            }
                        }
                        catch(Exception ex)
                        {
                            ex.printStackTrace();
                        }
                        break;
                    }

                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                    {
                        //retiring a VelocityTracker object back to be re-used by others.
                        velocityTracker1.recycle();
                        velocityTracker2.recycle();
                        power1 = 0;
                        power2 = 0;
                        break;
                    }
                }
            }
        }

        //HANDLING SINGLE TOUCH GESTURES
        else
        {
            switch (action)
            {
                case MotionEvent.ACTION_DOWN:
                {
                    resume();

                    if (velocityTracker1 == null)
                    {
                        //initializing a new VelocityTracker object to watch the velocity of a motion
                        velocityTracker1 = VelocityTracker.obtain();
                    }
                    else
                    {
                        //resetting the velocity tracker back to its initial state
                        velocityTracker1.clear();
                    }
                    //adding user's movement to the tracker.
                    velocityTracker1.addMovement(event);

                    //setting players positions
                    x = (int) event.getX();
                    y = (int) event.getY();
                    if (y > 0.5 * screenY)
                    {
                        //setting position
                        point1.set(x, y);
                        //setting speed/power
                        velocityTracker1 = VelocityTracker.obtain();
                        power1 = 0;
                        //retirning a VelocityTracker object back to be re-used by others.
                        velocityTracker1.recycle();
                    }
                    if (y < 0.5 * screenY)
                    {
                        //setting position
                        point2.set(x, y);
                        //setting speed/power
                        velocityTracker1 = VelocityTracker.obtain();
                        power2 = 0;
                        //retirning a VelocityTracker object back to be re-used by others.
                        velocityTracker1.recycle();
                    }
                    break;
                }

                case MotionEvent.ACTION_MOVE:
                {
                    velocityTracker1.addMovement(event);
                    //calculating velocity
                    velocityTracker1.computeCurrentVelocity(1000);
                    //setting players positions
                    x = (int) event.getX();
                    y = (int) event.getY();
                    if (y > 0.5 * screenY)
                    {
                        //setting position
                        point1.set(x, y);
                        //setting speed/power
                        velocityTracker1 = VelocityTracker.obtain();
                        //VelocityTrackerCompat.
                        power1 = Math.abs(3 * velocityTracker1.getXVelocity() + Math.abs(5 * velocityTracker1.getYVelocity()));
                        //retiring a VelocityTracker object back to be re-used by others.
                        velocityTracker1.recycle();
                    }
                    if (y < 0.5 * screenY)
                    {
                        //setting position
                        point2.set(x, y);
                        //setting speed/power
                        velocityTracker1 = VelocityTracker.obtain();
                        power2 = Math.abs(3 * velocityTracker1.getXVelocity() + Math.abs(5 * velocityTracker1.getYVelocity()));
                        //retiring a VelocityTracker object back to be re-used by others.
                        velocityTracker1.recycle();
                    }
                    break;
                }

                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                {
                    //resetting powers
                    power1 = 0;
                    power2 = 0;
                    break;
                }

            }
        }
        return true;
    }


    public void pause()
    {
        //pausing music
        if(!isPaused && !blueWon && !redWon)
        {
            isPaused = true;
            if(!DataStore.isMuted())
            {
                MainMenu.getMediaPlayer().pause();
            }
        }
        //resetting ball
        if(redWon || blueWon)
        {
            ball.reset();
            ball.stop();
        }
    }


    public void resume()
    {
        //resuming music
        if(isPaused)
        {
            if(!DataStore.isMuted())
            {
                MainMenu.getMediaPlayer().start();
            }
            isPaused = false;
        }

        //resetting game
        if(blueWon || redWon)
        {
            //resetting game field
            rocket1.setPoints(0);
            rocket2.setPoints(0);
            blueWon = false;
            redWon = false;
            ball.reset();
            ball.stop();
        }
    }


    public boolean isPaused() { return isPaused; }

    public boolean isRedWon() { return redWon; }

    public boolean isBlueWon() { return blueWon; }

}
package com.example.mateuszfijak.pongz;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;


public class Rocket
{
    //game type, which affects rocket shape
    private int gameType;

    //player apperance
    private RectF shape;
    private float sizeX;
    private float sizeY;
    private int color;
    private int borderColor;

    //game scores parameters
    private int points;
    private int lives;

    //screen size for proper size scalling
    private int screenX;
    private int screenY;


    public Rocket(int screenX, int screenY, int gameType, int color, int borderColor)
    {
        //initializing gametype
        this.gameType = gameType;

        //initializing screen size
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing player's body
        shape = new RectF();
        this.color = color;
        this.borderColor = borderColor;

        //initializing player's size
        if(gameType == 1)  //pong game
        {
            this.sizeX = screenX/4;
            this.sizeY = screenY/40;
        }
        if(gameType == 2)  //air hockey game
        {
            this.sizeX = screenX/5;
            this.sizeY = screenX/5;
        }

        //setting game parameters
        this.points = 0;
        this.lives = 3;
    }


    public Rocket(int screenX, int screenY, int gameType)
    {
        //initializing gametype
        this.gameType = gameType;

        //initializing screen size
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing player's body
        shape = new RectF();

        //initializing plyer's color
        int tempChoice = DataStore.getColorNumber();

        switch (tempChoice)
        {
            case 1:
            {
                this.color = Color.rgb(0,205,0);
                this.borderColor = Color.rgb(173,255,47);

                break;
            }
            case 2:
            {
                this.color = Color.rgb(30,144,255);
                this.borderColor = Color.rgb(135,206,235);
                break;
            }
            case 3:
            {
                this.color = Color.rgb(220,20,0);
                this.borderColor = Color.rgb(255,99,71);
                break;
            }
            case 4:
            {
                this.color = Color.rgb(255,255,255);
                this.borderColor = Color.rgb(248,248,255);
                break;
            }
            default:
            {
                this.color = Color.TRANSPARENT;
                this.borderColor = Color.TRANSPARENT;
                break;
            }
        }

        //initializing player's size
        if(gameType == 1)  //pong game
        {
            this.sizeX = screenX/4;
            this.sizeY = screenY/40;
        }
        if(gameType == 2)  //air hockey game
        {
            this.sizeX = screenX/5;
            this.sizeY = screenX/5;
        }

        //setting game parameters
        this.points = 0;
        this.lives = 3;
    }


    //updates rocket position to the center of the touched area
    public void update(Point point)
    {
        //pong game
        if(gameType == 1)
        {
            shape.left = (int) (point.x - sizeX/2);
            shape.top = (int) (point.y + sizeY/2);
            shape.right = (int)(shape.left + sizeX);
            shape.bottom = (int)(shape.top - sizeY);
        }

        //air hockey game
        if(gameType == 2)
        {
            //limiting x position
            if(0 + 0.55*sizeX < point.x && point.x < screenX - 0.55*sizeX)
            {
                shape.left = (int) (point.x - sizeX/2);
                shape.right = (int)(shape.left + sizeX);
            }
            if(0 + 0.55*sizeX > point.x)
            {
                shape.left = (int) (0.15*sizeX);
                shape.right = (int)(shape.left + sizeX);
            }
            if(point.x > screenX - 0.55*sizeX)
            {
                shape.left = (int) (screenX - 1.15*sizeX);
                shape.right = (int)(shape.left + sizeX);
            }
            //limiting y position
            if(point.y > sizeY/2)
            {
                if(0 + 0.55*sizeY < point.y && point.y < screenY - 0.55*sizeY)
                {
                    shape.top = (int) (point.y + sizeY/2);
                    shape.bottom = (int)(shape.top - sizeY);
                }
                if(point.y > screenY - 0.55*sizeY)
                {
                    shape.top = (int) (screenY - 0.15*sizeY);
                    shape.bottom = (int)(shape.top - sizeY);
                }
            }
            if(point.y < screenY/2)
            {
                if(0 + 0.55*sizeY < point.y && point.y < screenY - 0.55*sizeY)
                {
                    shape.top = (int) (point.y + sizeY/2);
                    shape.bottom = (int)(shape.top - sizeY);
                }
                if(shape.top < 0.15*sizeY)
                {
                    shape.top = (int) (0.15*sizeY);
                    shape.bottom = (int)(shape.top - sizeY);
                }
            }
        }
    }

    public void draw(Canvas canvas)
    {
        //initializing ball colorss+
        Paint paint1 = new Paint();
        paint1.setColor(borderColor);
        Paint paint2 = new Paint();
        paint2.setColor(color);

        //drawing edges
        RectF tempRect = new RectF(shape.left + sizeX/10, shape.top - sizeY/10, shape.right - sizeX/10, shape.bottom + sizeY/10);

        //drawing rects as ovals
        canvas.drawOval(shape, paint1);
        canvas.drawOval(tempRect, paint2);
    }


    public boolean collide(Ball ball)
    {
        //calculating distance between ball and rocket center
        double xDif = this.shape.centerX() - ball.getShape().centerX();
        double yDif = this.shape.centerY() - ball.getShape().centerY();
        double distanceSquared = Math.pow(xDif, 2) + Math.pow(yDif, 2);

        //return true if collision occurs
        return (distanceSquared < Math.pow(this.sizeX/2 + ball.getSizeY()/2, 2));
    }


    public RectF getShape() {return shape; }

    public int getPoints() { return points; }

    public void setPoints(int points) { this.points = points; }

    public int getLives() { return lives; }

    public void setLives(int lives) { this.lives = lives; }

    public float getSizeY() { return sizeY; }
}

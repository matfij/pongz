package com.example.mateuszfijak.pongz;

import android.graphics.Canvas;
import android.view.SurfaceHolder;


public class DrawThread extends Thread
{
    //fps limit
    private final int FPS = 100;

    //common drawing stuff
    private SurfaceHolder surfaceHolder;
    public static Canvas canvas;

    //game type
    private int gameType;
    private SoloEngine soloEngine;
    private DuoEngine duoEngine;
    private AirEngine airEngine;
    private RankEngine rankEngine;

    //running state
    private boolean running;


    //constructors for each game type

    public DrawThread(SurfaceHolder surfaceHolder, SoloEngine soloEngine)
    {
        super();
        this.surfaceHolder = surfaceHolder;
        this.soloEngine = soloEngine;
        this.gameType = 1;
    }

    public DrawThread(SurfaceHolder surfaceHolder, DuoEngine duoEngine)
    {
        super();
        this.surfaceHolder = surfaceHolder;
        this.duoEngine = duoEngine;
        this.gameType = 2;
    }

    public DrawThread(SurfaceHolder surfaceHolder, AirEngine airEngine)
    {
        super();
        this.surfaceHolder = surfaceHolder;
        this.airEngine = airEngine;
        this.gameType = 3;
    }

    public DrawThread(SurfaceHolder surfaceHolder, RankEngine rankEngine)
    {
        super();
        this.surfaceHolder = surfaceHolder;
        this.rankEngine = rankEngine;
        this.gameType = 4;
    }


    @Override
    public void run()
    {
        //initializing fsp limiting and counters
        long startTime;
        long endTime;
        long waitTime;
        long targetTime = 1000/FPS;

        while(running)
        {
            //starting timer
            startTime = System.nanoTime();

            try
            {
                //locking canvas
                canvas = this.surfaceHolder.lockCanvas();

                //redrawing game field
                switch (gameType)
                {
                    case 1:
                    {
                        this.soloEngine.draw(canvas);
                        break;
                    }

                    case 2:
                    {
                        this.duoEngine.draw(canvas);
                        break;
                    }

                    case 3:
                    {
                        this.airEngine.draw(canvas);
                        break;
                    }

                    case 4:
                    {
                        this.rankEngine.draw(canvas);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            finally
            {
                if(canvas != null)
                {
                    try
                    {
                        //unlocking canvas
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }

            //calculating this loop time
            endTime = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - endTime;

            //if the loop was shorter then target sleep to keep static fps
            try
            {
                this.sleep(waitTime);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public void setRunning(boolean b) { running = b; }
}
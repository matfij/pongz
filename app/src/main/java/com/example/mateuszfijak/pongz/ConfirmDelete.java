package com.example.mateuszfijak.pongz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;


public class ConfirmDelete extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_delete);

        //setting fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.noButton:
            {
            //going back to highscores
            Intent intent = new Intent(getApplicationContext(), HighScores.class);
            startActivity(intent);

            break;
            }

            //resetting highscores
            case R.id.yesButton:
            {
                //initializing output data objects
                FileOutputStream fileOutputStream = null;

                //trying to open file
                try
                {
                    fileOutputStream = openFileOutput(DataStore.getFileName(), MODE_PRIVATE);
                    fileOutputStream.write("".getBytes());

                    //displaying info message
                    Toast.makeText(this, "HighScores resetted!", Toast.LENGTH_SHORT).show();
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                finally
                {
                    try
                    {
                        fileOutputStream.close();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }

                //going back to highscores
                Intent intent = new Intent(getApplicationContext(), HighScores.class);
                startActivity(intent);

                break;
            }
        }
    }
}

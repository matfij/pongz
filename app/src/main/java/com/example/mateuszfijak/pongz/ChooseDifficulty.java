package com.example.mateuszfijak.pongz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

public class ChooseDifficulty extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //setting fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_difficulty);
    }


    @Override
    protected void onPause()
    {
        super.onPause();
        //cleaning up memory
        this.finish();
    }


    @Override
    public void onBackPressed()
    {
        //going back to menu
        Intent intent = new Intent(getApplicationContext(), MainMenu.class);
        startActivity(intent);
        //dont replay the music
        MainMenu.setIsPlaying(true);
    }


    public void menuClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imageButton1:
            {
                //starting game
                Intent intent = new Intent(getApplicationContext(), SoloGame.class);
                startActivity(intent);
                DataStore.setDif(1);
                //stopping menu music
                if(!DataStore.isMuted())
                {
                    MainMenu.getMediaPlayer().stop();
                    MainMenu.getMediaPlayer().release();
                }
                break;
            }

            case R.id.imageButton2:
            {
                //starting game
                Intent intent = new Intent(getApplicationContext(), SoloGame.class);
                startActivity(intent);
                DataStore.setDif(2);
                //stopping menu music
                if(!DataStore.isMuted())
                {
                    MainMenu.getMediaPlayer().stop();
                    MainMenu.getMediaPlayer().release();
                }
                break;
            }

            case R.id.imageButton3:
            {
                //starting game
                Intent intent = new Intent(getApplicationContext(), SoloGame.class);
                startActivity(intent);
                DataStore.setDif(3);
                //stopping menu music
                if(!DataStore.isMuted())
                {
                    MainMenu.getMediaPlayer().stop();
                    MainMenu.getMediaPlayer().release();
                }
                break;
            }
        }
    }
}

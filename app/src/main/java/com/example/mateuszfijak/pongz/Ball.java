package com.example.mateuszfijak.pongz;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.media.MediaPlayer;


public class Ball
{
    //game type, which affects ball bounce sound and mechanism
    private int gameType;

    //ball apperance
    private RectF shape;
    private float sizeX;
    private float sizeY;
    private int color;
    private int borderColor;

    //ball movement
    private double ang;
    private double vel;
    private boolean isStopped;

    //game difficulty, which affects ball speed
    private int difficulty;

    //screen size for proper size scalling
    private int screenX;
    private int screenY;

    //serve counter for multiplayer game
    private int counter;

    //music stuff
    private MediaPlayer sound;


    public Ball(int screenX, int screenY, Context context, int gameType, int difficulty, int color, int borderColor)
    {
        //initializing screen size
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing ball body
        this.shape = new RectF();
        this.color = color;
        this.borderColor = borderColor;

        //initializing difficulty
        this.difficulty = difficulty;

        //setting counter
        counter = 1;

        //initializing sound and size depends whether we play pong or air hockey
        this.gameType = gameType;
        if(gameType == 1)
        {
            sound = MediaPlayer.create(context, R.raw.bounce);
            sizeX = screenX/25;
            sizeY = screenX/25;
        }
        if(gameType == 2)
        {
            sound = MediaPlayer.create(context, R.raw.hit2);
            sizeX = screenX/12;
            sizeY = screenX/12;
        }

        //(re)setting ball
        reset();
    }


    //stopping ball movement / PONG & AIR HOCKEY
    public void stop()
    {
        //stopping ball movement
        vel = 0;
        isStopped = true;

        //moving ball to the middle
        shape.left = (int) (screenX/2 - sizeX/2);
        shape.top = (int) (screenY/2 + sizeY/2);
        shape.right = (int) (screenX/2 + sizeX/2);
        shape.bottom = (int) (screenY/2 - sizeY/2);
    }


    //stopping ball movement  / PONG
    public void stop2()
    {
        //stopping ball movement
        vel = 0;
        isStopped = true;

        //moving ball to the starting point
        if(counter % 2 == 0)
        {
            shape.left = (int) (screenX/2 - sizeX/2);
            shape.top = (int) (screenY/3 - sizeY);
            shape.right = (int) (screenX/2 + sizeX/2);
            shape.bottom = (int) (screenY/3);
        }
        if(counter % 2 == 1)
        {
            shape.left = (int) (screenX/2 - sizeX/2);
            shape.top = (int) (2*screenY/3);
            shape.right = (int) (screenX/2 + sizeX/2);
            shape.bottom = (int) (2*screenY/3 + sizeY);
        }
        counter++;
    }


    //resetting method for single player game  / PONG & AIR HOCKEY
    public void reset()
    {
        //setting ball heading angle
        ang = 0;

        //(re)initializing speed
        resetVel();
        isStopped = false;
    }


    //resetting method for multi player game  / PONG & AIR HOCKEY
    public void reset2()
    {
        //setting ball heading angle
        if(counter % 2 == 0)
        {
            ang = 180*Math.PI/180;
        }
        if(counter % 2 == 1)
        {
            ang = 0;
        }

        //(re)initializing speed
        resetVel();

        isStopped = false;
        counter++;
    }


    //updating ball position / PONG & AIR HOCKEY
    public void update(double fps)
    {
        shape.left = (int) (shape.left + (Math.sin(ang)*vel)/fps);
        shape.bottom = (int) (shape.bottom + (Math.cos(ang)*vel)/fps);
        shape.right = (int) (shape.left + sizeX);
        shape.top = (int) (shape.bottom + sizeY);

        //slowing down the ball in air hockey
        if(gameType == 2)
        {
            if(Math.abs(vel) > screenY/10)  vel -= vel/200;
        }
    }


    //drawing the ball / PONG & AIR HOCKEY
    public void draw(Canvas canvas)
    {
        //initializing ball colors
        Paint paint1 = new Paint();
        paint1.setColor(borderColor);
        Paint paint2 = new Paint();
        paint2.setColor(color);

        //drawing edges
        RectF tempRect = new RectF(shape.left + sizeX/10, shape.top - sizeX/10, shape.right - sizeX/10, shape.bottom + sizeX/10);

        //drawing rects as ovals
        canvas.drawOval(shape, paint1);
        canvas.drawOval(tempRect, paint2);
    }


    //reversing heading angel on side walls / PONG & AIR HOCKEY
    public void wallBounce()
    {
        ang = -ang;
        //offsetting the ball from the wall
        if(shape.centerX() < screenX/2) shape.offset(screenX/100, 0);
        if(shape.centerX() > screenX/2) shape.offset(-screenX/100, 0);

    }


    //reversing heading angel on top and bottom walls / AIR HOCKEY
    public void wallBounce2()
    {
        ang = 180*Math.PI/180 - ang;
        //offsetting the ball from the wall
        if(shape.centerY() < screenY/2) shape.offset(0, screenX/100);
        if(shape.centerY() > screenY/2) shape.offset(0, -screenX/100);
    }


    //increasing ball speed  / PONG
    public void speedUP()
    {
        vel += screenX/10;
    }


    //increasing ball speed based on hit power / AIR HOCKEY
    public void speedUP2(double power)
    {
        vel += Math.sqrt(20*power);
        if(vel < screenX/10)  vel+= screenX/8;
    }


    //bouncing after hitting the bat / PONG & AIR HOCKEY
    public void playerBounce(RectF batShape)
    {
        //if the sounds are not muted play bounce sound
        if(!DataStore.isSilent())
        {
            if(sound.isPlaying() && sound != null)
            {
                sound.pause();
            }
            sound.start();
        }

        //pong type game
        if(gameType == 1)
        {
            //the further the ball is on the side of each bat, the bigger the angle is
            if(shape.centerY() > screenY/2)
            {
                //reinitializing ball heading angle
                ang = 180*Math.PI/180 + ((batShape.centerX()-this.shape.centerX())/(0.01*batShape.width()))*Math.PI/180;
                //offsetting the ball from the bat
                shape.offset(0, -screenX/100);
            }
            if(shape.centerY() < screenY/2)
            {
                //reinitializing ball heading angle
                ang = 0*Math.PI/180 + ((-batShape.centerX()+this.shape.centerX())/(0.01*batShape.width()))*Math.PI/180;
                //offsetting the ball from the bat
                shape.offset(0, screenX/100);
            }
        }

        //air hockey type game
        if(gameType == 2)
        {
            if(shape.bottom > screenY/2)
            {
                if(this.shape.centerY() < batShape.centerY())
                {
                    //reinitializing ball heading angle
                    ang = 180*Math.PI/180 + ((batShape.centerX()-this.shape.centerX())/(0.01*batShape.width()))*Math.PI/180;
                    //offsetting the ball from the bat
                    if(shape.centerX() > batShape.centerX())  shape.offset(screenX/100, -screenX/100);
                    if(shape.centerX() < batShape.centerX())  shape.offset(-screenX/100, -screenX/100);
                }

                if(this.shape.centerY() > batShape.centerY())
                {
                    //reinitializing ball heading angle
                    ang = 0*Math.PI/180 + ((-batShape.centerX()+this.shape.centerX())/(0.01*batShape.width()))*Math.PI/180;
                    //offsetting the ball from the bat
                    if(shape.centerX() > batShape.centerX())  shape.offset(screenX/100, screenX/100);
                    if(shape.centerX() < batShape.centerX())  shape.offset(-screenX/100, screenX/100);
                }
            }
            if(shape.bottom < screenY/2)
            {
                if(this.shape.centerY() > batShape.centerY())
                {
                    //reinitializing ball heading angle
                    ang = 0*Math.PI/180 + ((-batShape.centerX()+this.shape.centerX())/(0.01*batShape.width()))*Math.PI/180;
                    //ofsetting the ball from the bat
                    if(shape.centerX() > batShape.centerX())  shape.offset(screenX/100, screenX/100);
                    if(shape.centerX() < batShape.centerX())  shape.offset(-screenX/100, screenX/100);
                }

                if(this.shape.centerY() < batShape.centerY())
                {
                    //reinitializing ball heading angle
                    ang = 180*Math.PI/180 + ((batShape.centerX()-this.shape.centerX())/(0.01*batShape.width()))*Math.PI/180;
                    //ofsetting the ball from the bat
                    if(shape.centerX() > batShape.centerX())  shape.offset(screenX/100, -screenX/100);
                    if(shape.centerX() < batShape.centerX())  shape.offset(-screenX/100, -screenX/100);
                }
            }
        }
    }


    //releasing mediaplayer to prevent lack of cache / PONG & AIR HOCKEY
    public void clear() { sound.release(); }


    // resetting ball velocity / PONG & AIR HOCKEY
    public void resetVel()
    {
        switch(difficulty)
        {
            case 1:
            {
                vel = 0.8*screenX;
                break;
            }
            case 2:
            {
                vel = 1.4*screenX;
                break;
            }
            case 3:
            {
                vel = 2*screenX;
                break;
            }
        }
    }


    //changing ball color
    public void changeColor(int color1, int color2)
    {
        this.color = color1;
        this.borderColor = color2;
    }


    public RectF getShape() { return shape; }

    public float getSizeX() { return sizeX; }

    public float getSizeY() { return sizeY; }

    public double getAng() { return ang; }

    public void setDifficulty(int difficulty) { this.difficulty = difficulty; }

    public double getVel() { return vel; }

    public boolean isStopped() { return isStopped; }

    public void setStopped(boolean stopped) { isStopped = stopped; }
}
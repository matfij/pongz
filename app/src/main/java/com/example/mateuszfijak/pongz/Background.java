package com.example.mateuszfijak.pongz;

import android.graphics.Bitmap;
import android.graphics.Canvas;


public class Background
{
    //bitmap images
    private Bitmap bitmap1;
    private Bitmap bitmap2;

    //moving image
    private int yPos;
    private double speedY;

    //screen size for scalling stuff
    private int screenY;


    public Background(Bitmap bitmap1, Bitmap bitmap2, int screenY)
    {
        //initializing bitmap images
        this.bitmap1 = bitmap1;
        this.bitmap2 = bitmap2;

        //initializing moving speed
        this.speedY = screenY/5;
        this.yPos = 0;

        //initializing screen height
        this.screenY = screenY;
    }


    public void update(int fps)
    {
        //moving 2nd layer of background till the end of the screen
        yPos -= speedY/fps;

        //if the active layer is in the end of the screen reset it
        if(yPos < -1.6*screenY)
        {
            yPos = 0;
        }
    }


    public void draw(Canvas canvas)
    {
        //drawing backgound layers
        canvas.drawBitmap(bitmap1,0,0,null);
        canvas.drawBitmap(bitmap2, 0, yPos,null);
    }


    public void clear()
    {
        //clearing cache
        bitmap1.recycle();
        bitmap2.recycle();
    }
}

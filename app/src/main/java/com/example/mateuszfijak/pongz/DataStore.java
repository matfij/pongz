package com.example.mateuszfijak.pongz;


public class DataStore
{
    //holding difficulty choice
    private static int dif;

    //holding current ranked game scores
    private static int scores;

    //holding music and sound status
    private static boolean muted;
    private static boolean silent;

    //holding rocket color
    private static int colorNumber = 1;

    //holding highscores file name
    private static final String fileName = "ranking.txt";


    public static int getDif() {
        return dif;
    }

    public static  int getScores() {
        return scores;
    }

    public static boolean isMuted() { return muted; }

    public static boolean isSilent() { return silent; }

    public static int getColorNumber() { return colorNumber; }

    public static String getFileName() { return fileName; }

    public static void setDif(int diff) { dif = diff; }

    public static void setScores(int scoress) { scores = scoress; }

    public static void setMuted(boolean mutedd) { muted = mutedd; }

    public static void setSilent(boolean silentt) { silent = silentt; }

    public static void setColorNumber(int colorNumberr) { colorNumber = colorNumberr; }

}
